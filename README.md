# ETL project to process data of recipes 

El siguiente proyecto utiliza scripts ETL para transformar un dataset de recetas de cocina, el cual tiene una base de datos fuente en varios archivos .csv que se pretenden almacenar en una datawarehouse en mysql.
De la misma manera, se propende limpiar algunas datos ya sea eliminando, emputando o editando con la intención de mejorar la calidad de vida de la base de datos para posteriormente realizar analíica con los mismos.


![Schema recipe](schema/Selecci%C3%B3n_069.png)

A continuación se presentan las tablas:

**pp_users:**
- u: INT
- techniques: VARCHAR
- mean_ratings: DECIMAL (los ratings no tenía mucha variabilidad, así que se sacó la media como representación única)

**Interactions:**
- user_id: INT (fue eliminado, pues contenía datos exageradamente grande que no correspondía con PK de usuarios)
- recipe_id: INT
- date: VARCHAR
- rating: INT
- i: INT
- u: INT

**Raw_recipie:**  (Recipes)
- id: INT
- name: VARCHAR
- minutes: INT
- contributor_id: INT
- submitted: VARCHAR
- description: VARCHAR
- nutri_c1: DECIMAL
- nutri_c2: DECIMAL
- nutri_c3: DECIMAL
- nutri_c4: DECIMAL
- nutri_c5: DECIMAL
- nutri_c6: DECIMAL
- nutri_c7: DECIMAL

Considerando que “nutrition” contiene siempre una lista de 7 números, se cree que posiblemente corresponda a diferentes componente como azúcares, carbohidratos, etc. de manera que se dividieron en columnas con el fin de que sea más fácil de analizar y procesar.

**Tags:**
- tag_id: INT
- name: VARCHAR

**Steps:**
- step_id: int
- name: VARCHAR

dado que los “steps” no se repiten entre recetas, no conviene crear una relación de muchos a muchos.

**Ingr_map:**  (ingredients)
- ingr_id: INT
- name: VARCHAR
- replaced: VARCHAR
- count: VARCHAR
- id: INT


Este repositorio incluye:
* ``schema``. Contiene el modelo generado en workbench y el sql para generarlo.
* ``recipe_etl/etl`` Contiene scripts ETL los scripts por cada una de las tablas principales
* ``recipe_etl/source_data`` Aquí están todos los .csv que son los datos fuente.
* ``recipe_etl/db_credentials`` configuración para la conexión a MySQL y paths relativos a los datos fuente.
* ``recipe_etl/sql_queries`` Consultas sql para la DataWareHouse
* ``recipe_etl/variables`` Variables del sistema y nombre de la DB
* ``recipe_etl/main`` Desde aquí se ejecuta todo el projecto, llamando a la función PIPELINE quien ejecuta los etl de forma ordenada para evitar errores de dependencias.
