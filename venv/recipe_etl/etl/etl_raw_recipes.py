import csv
import statistics
from mysql.connector import Error
from db_credentials import csv_sources
from variables import datawarehouse_name
from sql_queries import mysql_insert_steps

class RawRecipesEtl:

    def __init__(self, csv_path, query, conn):
        self.csv_path = csv_path
        self.conn = conn
        self.cursor = self.conn.cursor()
        self.query = query
        self.message_errors = []

    def __extract_transform_load(self):

        with open(csv_sources[self.csv_path]) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                self.__transform(row)   # Transform

    def __transform(self, row):

        row["id"] = int(row["id"])
        row["minutes"] = int(row["minutes"])
        row["contributor_id"] = int(row["contributor_id"])
        row["description"] = row["description"][:300]

        nutririon = [float(i) for i in row["nutrition"][1:-1].split(",")]

        row_values = (row["id"], row["name"], row["minutes"], row["contributor_id"], row["submitted"], row["description"],
                        nutririon[0], nutririon[1], nutririon[2], nutririon[3], nutririon[4], nutririon[5], nutririon[6])

        self.__load_one_by_one(row_values)


        steps_ = row["steps"].replace("'", "")[1:-1].split(",")

        # Creating new table STEPS - LOAD
        try:
            for st in steps_:
                self.cursor.execute(mysql_insert_steps, (st[:299], row["id"]))
        except Error as error:
            self.message_errors.append("STEP: ", error)


        # Creating new table TAGS - LOAD

        try:
            for st in steps_:
                self.cursor.execute(mysql_insert_steps, (st[:299], row["id"]))
        except Error as error:
            self.message_errors.append("STEP: ", error)



    def __load_one_by_one(self, row):
        try:
            self.cursor.execute(self.query, row)

        except Error as error:
            self.message_errors.append(error)

        except Exception as e:
            print(e.args)


    def etl_process(self):

        self.__extract_transform_load()
        self.conn.commit()
        self.cursor.close()
