import csv
import pickle
import pandas

import statistics
from mysql.connector import Error
from db_credentials import csv_sources

class IngredientsEtl:

    def __init__(self, csv_path, query, conn):
        self.csv_path = csv_path
        self.conn = conn
        self.cursor = self.conn.cursor()
        self.query = query
        self.message_errors = []

    def __extract_transform_load(self):

        with open(csv_sources[self.csv_path], 'rb') as f:
            ingred_pkl = pickle.load(f)

            for i in range(ingred_pkl.shape[0]):
                self.__transform(ingred_pkl.iloc[i])

    def __transform(self, row):
        row["raw_ingr"] = row["raw_ingr"]
        row["replaced"] = row["replaced"]
        row["count"] = int(row["count"])
        row["id"] = int(row["id"])

        row_values = (row["raw_ingr"], row["replaced"], row["count"], row["id"])
        self.__load_one_by_one(row_values)

    def __load_one_by_one(self, row):
        try:
            self.cursor.execute(self.query, row)

        except Error as error:
            self.message_errors.append(error)

        except Exception as e:
            print(e.args)


    def etl_process(self):

        self.__extract_transform_load()
        self.conn.commit()
        self.cursor.close()





        # Creating new table TAGS - LOAD
"""
        try:
            for st in steps_:
                self.cursor.execute(mysql_insert_steps, (st[:299], row["id"]))
        except Error as error:
            self.message_errors.append("STEP: ", error)
"""