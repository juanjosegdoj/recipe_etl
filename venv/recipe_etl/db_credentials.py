from variables import datawarehouse_name

# mysql (datawarehouse)
datawarehouse_db_config = {
  'host': 'localhost',
  'user': 'root',
  'password': 'admin123',
  'db': '{}'.format(datawarehouse_name)
}

# csv (source)
csv_sources = {
  "ingredients": "source_data/ingr_map.pkl",
  "pp_users": "source_data/PP_users.csv",
  "interactions_test": "source_data/interactions_test.csv",
  "interactions_train": "source_data/interactions_train.csv",
  "interactions_validation": "source_data/interactions_validation.csv",
  "pp_recipes": "source_data/PP_recipes.csv",
  "raw_interactions": "source_data/RAW_interactions.csv",
  "raw_recipes": "source_data/RAW_recipes.csv"
}