import mysql.connector

from db_credentials import datawarehouse_db_config
from sql_queries import mysql_insert_raw_recipie, mysql_insert_pp_user, mysql_insert_pp_interaction, mysql_insert_ingredients
from etl import etl_users, etl_raw_recipes, etl_interactions, etl_ingredients


def pipeline():
    """
    Here are configured how you must run ETLs, because there are dependencies on each other, so, if you modifie
    could have problems of depencies.
    :return:
    """
    conn = mysql.connector.connect(**datawarehouse_db_config)  # name of the data base

    if not conn.is_connected():
        raise Exception("It was not possible connect to Datawarehouse")

    etl_users.UserEtl("pp_users", mysql_insert_pp_user, conn).etl_process() # Users
    etl_raw_recipes.RawRecipesEtl("raw_recipes", mysql_insert_raw_recipie, conn).etl_process()  # Recipes
    etl_interactions.InteractionEtl("interactions_test", mysql_insert_pp_interaction, conn).etl_process()
    etl_interactions.InteractionEtl("interactions_train", mysql_insert_pp_interaction, conn).etl_process()
    etl_interactions.InteractionEtl("interactions_validation", mysql_insert_pp_interaction, conn).etl_process()
    etl_ingredients.IngredientsEtl("ingredients", mysql_insert_ingredients, conn).etl_process()

    conn.close()

if __name__ == "__main__":
    pipeline()

